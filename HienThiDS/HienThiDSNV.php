<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        
        //Lựa chọn cơ sở dữ liệu
        mysqli_select_db($link, "dulieu");

        //Khai báo chuỗi SQL dạng Select
        $sql = "Select * from nhanvien";
        //Thực hiện truy vấn
        //Lưu kết quả vào biến result
        // $result = mysqli_query($sql, $link);
        $result = mysqli_query($link, $sql);
        echo '<table border = "1" width = "100%">';
        echo '<caption> Danh Sách Nhân Viên</caption>';
        //In tiêu đề của bảng
        echo "
            <tr>
                <td> IDNV </td>
                <td> Họ tên </td>
                <td> IDPB </td>
                <td> Địa chỉ </td>
            </tr>
        ";
        while($row = mysqli_fetch_array($result))
        {
            //CÁCH 1
            // $maso = $row["maso"];
            // $hoten = $row["hoten"];
            // $ngaysinh = $row["ngaysinh"];
            // $nghenghiep = $row["nghenghiep"];
            // echo "
            //     <tr>
            //         <td>$maso</td>
            //         <td>$hoten</td>
            //         <td>$ngaysinh</td>
            //         <td>$nghenghiep</td>
            //     </tr>
            // ";

            //CÁCH 2
            echo '<tr><td>'.$row['IDNV'].'</td><td>'.$row['Hoten'].'</td><td>'.$row['IDPB'].'</td><td>'.$row['Diachi'].'</td></tr>';
        }
        echo "</TABLE>";

        //Giải phóng tập các bản ghi trong $result
        mysqli_free_result($result);
        mysqli_close($link);
    ?>
</body>
</html>